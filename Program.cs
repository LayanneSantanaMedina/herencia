using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    /*
    class Vehicle
    {
        public string brand = "Ford"; //Vehicle field
        public void honk()   //Vehicle method
        {
            Console.WriteLine("Tuut, tuut!");
        }
    }

    class Car : Vehicle  //derived class (child)
    {
        public string modelName = "Mustang";   //Car field
    }*/


    class Program
    {
        static void Main(string[] args)
        {
            /*///create a myCar object
            Car myCar = new Car();

            //Call the honk ()method (From the Vehicle class) on the myCar object
            myCar.honk();


            Console.WriteLine(myCar.brand + " " + myCar.modelName);
            
            Console.ReadKey();
             */



            //CLASE OPERACIONES
            Suma suma1 = new Suma();
            suma1.Valor1 = 10;
            suma1.Valor2 = 7;
            suma1.Operar();
            Console.WriteLine("La suma de " + suma1.Valor1 + " y " +
              suma1.Valor2 + " es " + suma1.Resultado);
            Resta resta1 = new Resta();
            resta1.Valor1 = 8;
            resta1.Valor2 = 4;
            resta1.Operar();
            Console.WriteLine("La diferencia de " + resta1.Valor1 +
                  " y " + resta1.Valor2 + " es " + resta1.Resultado);

            Console.ReadKey();

        }
    }
}

